module Analysis ( guessSingleXor
                , solveVigenere
                , countIdenticalBlocks
                , blockCipherModeOracle
                , byteByByteECBCracker
                , byteByByteECBOracle
                ) where

import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import Data.List (find, foldl')
import Data.Word8 (Word8)
import Data.Foldable (maximumBy, minimumBy, forM_)
import Data.Bits (xor, testBit, finiteBitSize)
import Data.Text (Text)
import qualified Data.Text as T
import Encryption (encryptAES128ECB)
import Util (BlockCipherMode(..), splitEvery, blocks, readBase64, stripCtrl)
import Data.Text.Encoding (decodeUtf8')
import Data.Either.Combinators (rightToMaybe)

guessSingleXor :: ByteString -> ByteString
guessSingleXor bs =
    let mostCommon = mostCommonChar bs
        space = 32 :: Word8
        diff = mostCommon `xor` space
    in  BS.pack $ (`xor` diff) <$> BS.unpack bs

countIdenticalBlocks :: Int -> ByteString -> Int
countIdenticalBlocks blockSize ciphertext =
  let theBlocks = blocks blockSize ciphertext
      blockPairs = getBlockPairs theBlocks
  in  length . filter (uncurry (==)) $ blockPairs
  where
    getBlockPairs :: [a] -> [(a, a)]
    getBlockPairs [] = []
    getBlockPairs (x:xs) = zip (repeat x) xs ++ getBlockPairs xs

solveVigenere :: ByteString -> ByteString
solveVigenere ciphertext = solveVigenereByKeysize ciphertext (bestKeysize ciphertext 2 40)

solveVigenereByKeysize :: ByteString -> Int -> ByteString
solveVigenereByKeysize ciphertext keysize =
  let transposed = transposedBlocks ciphertext keysize
      solvedBlocks = guessSingleXor <$> transposed
  in  BS.concat $ BS.transpose solvedBlocks

transposedBlocks :: ByteString -> Int -> [ByteString]
transposedBlocks ciphertext size =
  let blocks = BS.pack <$> splitEvery size (BS.unpack ciphertext)
  in  BS.transpose blocks

bestKeysize :: ByteString -> Int -> Int -> Int
bestKeysize ciphertext rangeStart rangeEnd =
  let range = [rangeStart..rangeEnd]
  in  minimumBy (\x y -> compare (keysizeScore ciphertext x) (keysizeScore ciphertext y)) range

keysizeScore :: ByteString -> Int -> Double
keysizeScore ciphertext keysize =
  let keysize64 = fromIntegral keysize
      theBlocks = blocks keysize ciphertext
      blockPairs = zip theBlocks (tail theBlocks)
      absDistance = foldr (\(block1, block2) sum -> sum + hammingDistance block1 block2) 0 blockPairs
  in  fromIntegral absDistance / fromIntegral (length theBlocks * keysize)

hammingDistance :: ByteString -> ByteString -> Int
hammingDistance bs1 bs2 = countDistance (bits bs1) (bits bs2)
  where
    bits :: ByteString -> [Bool]
    bits bs = let words = BS.unpack bs in bits' =<< words

    bits' :: Word8 -> [Bool]
    bits' w = [testBit w i | i <- [0 .. finiteBitSize w - 1]]

    countDistance :: [Bool] -> [Bool] -> Int
    countDistance [] [] = 0
    countDistance (x : xs) [] = countDistance xs [] + 1
    countDistance [] (y : ys) = countDistance ys [] + 1
    countDistance (x : xs) (y : ys) =
      let singleDiff = if x == y then 0 else 1
      in  countDistance xs ys + singleDiff

mostCommonChar :: ByteString -> Word8
mostCommonChar bs = fst $ maximumBy (\(_, a) (_, b) -> compare a b) $ charFreqs bs

charFreqs :: ByteString -> [(Word8, Integer)]
charFreqs bs =
    let bytes = BS.unpack bs
    in  M.toList $ count bytes
  where
    count :: [Word8] -> HashMap Word8 Integer
    count bytes = count' bytes M.empty

    count' :: [Word8] -> HashMap Word8 Integer -> HashMap Word8 Integer
    count' [] map = map
    count' (x : xs) map =
        let oldCount = M.findWithDefault 0 x map
            newMap = M.insert x (oldCount + 1) map
        in  count' xs newMap

-- receives an unknown, randomised block encryption function and determines the mode it's using
-- very dumb, just assumes it's either ECB (with block size 16) or CBC
blockCipherModeOracle :: (ByteString -> IO (Maybe ByteString)) -> IO (Maybe BlockCipherMode)
blockCipherModeOracle encrypt = do
  -- test input should contain repeated blocks
  let testInput = BS.concat $ replicate 160 "A"
  result <- encrypt testInput
  return $ case result of
    Nothing -> Nothing
    Just encrypted ->
      let identicalBlocks = countIdenticalBlocks 16 encrypted
      in  Just $ if identicalBlocks >= 10 then ECB else CBC

byteByByteECBCracker :: (ByteString -> Maybe ByteString) -> Maybe Text
byteByByteECBCracker oracle = do
  blockSize <- determineBlockSize
  blockCount <- (\x -> BS.length x `div` blockSize) <$> oracle ""
  let (_, blocks) = foldl' (crackNextBlock blockSize) (replicate blockSize 0, []) [0..(blockCount-1)]
  stripCtrl <$> rightToMaybe (decodeUtf8' $ BS.pack blocks)
  where
    determineBlockSize :: Maybe Int
    determineBlockSize =
      let possibleSizes = [1..100]
      in  find isBlockSize possibleSizes
      where
        isBlockSize :: Int -> Bool
        isBlockSize size =
          case oracle $ BS.concat $ replicate (3 * size) "A" of
            Nothing -> False
            Just enc ->
              let bytes = BS.unpack enc
                  firstBlock = take size bytes
                  secondBlock = take size $ drop size bytes
                  thirdBlock = take size $ drop (2 * size) bytes
              in  firstBlock == secondBlock && secondBlock == thirdBlock

    guessByte :: Int -> Int -> [Word8] -> [Word8] -> Maybe Word8
    guessByte blockSize blockIndex prevBlock knownBytes = do
      let possibleBytes = [0..255] :: [Word8]
      let prefix = drop (length knownBytes + 1) prevBlock
      guessesByFirstEncByte <- M.fromList <$> mapM (performGuess prefix) possibleBytes
      actual <- nthBlock blockSize blockIndex . BS.unpack <$> oracle (BS.pack prefix)
      M.lookup actual guessesByFirstEncByte
      where
        nthBlock :: Int -> Int -> [Word8] -> [Word8]
        nthBlock blockSize n blocks = take blockSize $ drop (n * blockSize) blocks

        performGuess :: [Word8] -> Word8 -> Maybe ([Word8], Word8)
        performGuess prefix guess = do
          enc <- oracle (BS.pack (prefix ++ knownBytes ++ [guess]))
          let firstEncByte = nthBlock blockSize 0 $ BS.unpack enc
          return (firstEncByte, guess)

    crackBlock :: Int -> Int -> [Word8] -> [Word8]
    crackBlock blockSize blockIndex prevBlock = foldl' doFold [] [1..blockSize]
      where
        doFold :: [Word8] -> Int -> [Word8]
        doFold acc _ =
          case guessByte blockSize blockIndex prevBlock acc of
            Nothing -> acc -- just ignore errors, might happen at end of input
            Just byte -> acc ++ [byte]

    crackNextBlock :: Int -> ([Word8], [Word8]) -> Int -> ([Word8], [Word8])
    crackNextBlock blockSize (prevBlock, blocks) i =
      let cracked = crackBlock blockSize i prevBlock
      in  (cracked, blocks ++ cracked)

byteByByteECBOracle :: ByteString -> ByteString -> ByteString -> Maybe ByteString
byteByByteECBOracle key plaintext prefix =
  encryptAES128ECB key (BS.concat [prefix, plaintext])