module Encryption ( vigenereEncrypt
                  , pkcs7Pad
                  , encryptAES128ECB
                  , encryptAES128CBC
                  , randomAESEncrypt
                  , randomAESEncryptWithMode
                  ) where

import Control.Monad.Random (Rand, evalRand, getRandom, getRandomR, initStdGen, randomIO)
import Crypto.Cipher.AES (AES128)
import Crypto.Cipher.Types (cipherInit, ecbEncrypt)
import Crypto.Error (maybeCryptoError)
import Crypto.Random (DRG, MonadPseudoRandom, getRandomBytes, withDRG, drgNew)
import Data.ByteString (ByteString, pack, unpack)
import qualified Data.ByteString as BS
import qualified Data.Bits as Bits
import Data.List (foldl')
import System.Random (StdGen)
import Util (BlockCipherMode(..), blocks, xor)

vigenereEncrypt :: ByteString -> ByteString -> ByteString
vigenereEncrypt msg key = pack $ zipWith Bits.xor (unpack msg) (cycle (unpack key))

pkcs7Pad :: Int -> ByteString -> ByteString
pkcs7Pad blockSize input =
  let padLength = (blockSize - (BS.length input `mod` blockSize)) `mod` blockSize
      pad = BS.replicate padLength (fromIntegral padLength)
  in  BS.append input pad

encryptAES128ECB :: ByteString -> ByteString -> Maybe ByteString
encryptAES128ECB key plaintext = do
  aes <- maybeCryptoError $ cipherInit key :: Maybe AES128
  let padded = pkcs7Pad 16 plaintext
  return $ ecbEncrypt aes padded

encryptAES128CBC :: ByteString -> ByteString -> ByteString -> Maybe ByteString
encryptAES128CBC key iv plaintext = do
  aes <- maybeCryptoError $ cipherInit key :: Maybe AES128
  let padded = pkcs7Pad 16 plaintext
  let theBlocks = blocks 16 padded
  let encryptedBlocks = snd $ foldl' (chainEncrypt aes) (iv, []) theBlocks
  return $ BS.concat encryptedBlocks
  where
    chainEncrypt :: AES128 -> (ByteString, [ByteString]) -> ByteString -> (ByteString, [ByteString])
    chainEncrypt key (prevBlock, result) block = 
      let encryptedBlock = ecbEncrypt key $ prevBlock `xor` block
      in  (encryptedBlock, result ++ [encryptedBlock])

-- randomly encrypt input with:
-- * random 5-10 byte prefix
-- * random 5-10 byte suffix
-- * randomly choose ECB or CBC mode
-- * random key and (if necessary) IV
randomAESEncrypt :: ByteString -> IO (Maybe ByteString)
randomAESEncrypt input = do
  isECB <- randomIO :: IO Bool
  let mode = if isECB then ECB else CBC
  randomAESEncryptWithMode mode input

randomAESEncryptWithMode :: BlockCipherMode -> ByteString -> IO (Maybe ByteString)
randomAESEncryptWithMode mode input = do
  stdGen <- initStdGen
  cryptoGen <- drgNew
  return $ randomAESEncrypt' stdGen cryptoGen mode input

randomAESEncrypt' :: DRG cryptoRGT => StdGen -> cryptoRGT -> BlockCipherMode -> ByteString -> Maybe ByteString
randomAESEncrypt' stdRG cryptoRG mode input =
  let (prefixLength, suffixLength) = evalRand stdGenValues stdRG
      ((prefix, suffix, key, iv), _) = withDRG cryptoRG (cryptoRandValues prefixLength suffixLength)
      paddedInput = BS.concat [prefix, input, suffix]
  in  case mode of
        ECB -> encryptAES128ECB key input
        CBC -> encryptAES128CBC key iv paddedInput
  where
    stdGenValues :: Rand StdGen (Int, Int)
    stdGenValues = do
      prefixLength <- getRandomR (5 :: Int, 10 :: Int)
      suffixLength <- getRandomR (5 :: Int, 10 :: Int)
      return (prefixLength, suffixLength)

    cryptoRandValues :: DRG gen => Int -> Int -> MonadPseudoRandom gen (ByteString, ByteString, ByteString, ByteString)
    cryptoRandValues prefixLength suffixLength = do
      prefix <- getRandomBytes prefixLength
      suffix <- getRandomBytes suffixLength
      key <- getRandomBytes 16
      iv <- getRandomBytes 16
      return (prefix, suffix, key, iv)