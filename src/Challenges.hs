module Challenges
  ( Challenge
  , challenges
  ) where

import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Char (isPrint)
import Data.Either.Combinators (rightToMaybe)
import Data.List (find, maximumBy)
import Data.Map.Ordered (OMap)
import qualified Data.Map.Ordered as OM
import Data.Maybe (catMaybes)
import Data.Ord (comparing)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8, decodeUtf8')
import Data.Word8 (Word8)
import Analysis
import Decryption
import Encryption
import Util

type Challenge = IO (Maybe Text)

challenges :: [Challenge]
challenges = [ challenge1
             , challenge2
             , challenge3
             , challenge4
             , challenge5
             , challenge6
             , challenge7
             , challenge8
             , challenge9
             , challenge10
             , challenge11
             , challenge12
             , challenge13
             ]

challenge1 :: Challenge
challenge1 = return $ hex2Base64 "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"

challenge2 :: Challenge
challenge2 = return $ xorHex "1c0111001f010100061a024b53535009181c" "686974207468652062756c6c277320657965"

challenge3 :: Challenge
challenge3 = return $ guessSingleXorHex "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

challenge4 :: Challenge
challenge4 = selectAndDecryptValidLine <$> readFile "data/single-xor.txt"
  where
    selectAndDecryptValidLine :: String -> Maybe Text
    selectAndDecryptValidLine input =
      let textLines = T.pack <$> lines input
          attemptedDecryptions = T.strip <$> catMaybes (guessSingleXorHex <$> textLines)
      in  find isValidText attemptedDecryptions

    isValidText :: Text -> Bool
    isValidText text = all isPrint $ T.unpack text

challenge5 :: Challenge
challenge5 = let result = writeHex $ vigenereEncrypt (encodeUtf8 challenge5Input) (encodeUtf8 "ICE")
             in  return (return result)
  where
    challenge5Input :: Text
    challenge5Input = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"

challenge6 :: Challenge
challenge6 = trySolve <$> readFileBase64 "data/repeat-key-xor.txt"
  where
    trySolve :: Maybe ByteString -> Maybe Text
    trySolve input = do
      input' <- input
      rightToMaybe $ decodeUtf8' $ solveVigenere input'

challenge7 :: Challenge
challenge7 = do
  ciphertext <- readFileBase64 "data/aes-ecb.txt"
  return $ decryptAES128ECB "YELLOW SUBMARINE" =<< ciphertext

challenge8 :: Challenge
challenge8 = do
  ciphertexts <- readLinesBase64 "data/detect-aes-ecb.txt"
  let scores = countIdenticalBlocks 16 <$> ciphertexts
  let maxIndex = snd $ maximumBy (comparing fst) (zip scores [0..])
  return $ Just $ T.pack $ show maxIndex

challenge9 :: Challenge
challenge9 = return $ rightToMaybe $ decodeUtf8' $ pkcs7Pad 20 "YELLOW SUBMARINE"

challenge10 :: Challenge
challenge10 = do
  ciphertext <- readFileBase64 "data/aes-cbc.txt"
  let key = "YELLOW SUBMARINE" :: ByteString
  let iv = BS.pack $ replicate 16 (0 :: Word8)
  return $ decryptAES128CBC key iv =<< ciphertext

challenge11 :: Challenge
challenge11 = do
  Just ecbResult <- blockCipherModeOracle (randomAESEncryptWithMode ECB)
  Just cbcResult <- blockCipherModeOracle (randomAESEncryptWithMode CBC)
  Just randResult <- blockCipherModeOracle randomAESEncrypt
  return $ Just $ T.pack $
    "1: Expected: ECB - Actual: " ++ show ecbResult ++ "\n2: Expected: CBC - Actual: " ++ show cbcResult ++ "\n3: Expected: ? - Actual: " ++ show randResult

challenge12 :: Challenge
challenge12 = do
  let key = "YELLOW SUBMARINE" -- doesn't matter what key
  let Just pt = readBase64 "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
  let oracle = byteByByteECBOracle key pt
  return $ byteByByteECBCracker oracle

challenge13 :: Challenge
challenge13 = return $ do
  let key = "YELLOW SUBMARINE" -- doesn't matter what key
  forgedProfile <- forgeAdminProfile (encProfileFor key) (readProfile key)
  OM.lookup "role" forgedProfile

guessSingleXorHex :: Text -> Maybe Text
guessSingleXorHex x = do
  x' <- readHex x
  rightToMaybe $ decodeUtf8' $ guessSingleXor x'

type Profile = OMap Text Text

encProfileFor :: ByteString -> Text -> Maybe ByteString
encProfileFor key email =
  let fullProfile = OM.fromList [("email", email), ("uid", "10"), ("role", "user")]
      encoded = toQueryString fullProfile
  in  encryptAES128ECB key (encodeUtf8 encoded)

readProfile :: ByteString -> ByteString -> Maybe Profile
readProfile key encProfile = do
  decrypted <- decryptAES128ECB key encProfile
  fromQueryString decrypted

forgeAdminProfile :: (Text -> Maybe ByteString) -> (ByteString -> Maybe Profile) -> Maybe Profile
forgeAdminProfile encryptor decryptor = do
  part1 <- BS.concat . take 2 . blocks 16 <$> encryptor "abc@gmail.com"
  part2 <- BS.concat . take 1 . drop 1 .blocks 16 <$> encryptor "abcdefghijadmin\v\v\v\v\v\v\v\v\v\v\v"
  decryptor (part1 <> part2)
