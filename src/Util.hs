module Util
    ( BlockCipherMode(..)
    , hex2Base64
    , readHex
    , writeHex
    , xor
    , xorHex
    , readBase64
    , readFileBase64
    , readLinesBase64
    , splitEvery
    , blocks
    , stripCtrl
    , fromQueryString
    , toQueryString
    ) where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding.Base16 (decodeBase16, encodeBase16)
import Data.Text.Encoding.Base64 (encodeBase64)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Data.ByteString (ByteString, pack)
import qualified Data.ByteString as BS
import Data.ByteString.Base64 (decodeBase64)
import qualified Data.Bits as Bits
import Data.Either.Combinators (rightToMaybe)
import Data.Map.Ordered (OMap)
import qualified Data.Map.Ordered as OM
import Data.Maybe (catMaybes)
import Data.Char (isControl)

data BlockCipherMode = ECB | CBC deriving (Eq, Show)

splitEvery :: Int -> [a] -> [[a]]
splitEvery n = takeWhile (not . null) . map (take n) . iterate (drop n)

blocks :: Int -> ByteString -> [ByteString]
blocks byteSize input =
  let bytes = BS.unpack input
      grouped = splitEvery byteSize bytes
  in  BS.pack <$> grouped

hex2Base64 :: Text -> Maybe Text
hex2Base64 hex = rightToMaybe $ do
    unicode <- decodeBase16 hex
    return $ encodeBase64 unicode

readHex :: Text -> Maybe ByteString
readHex hex = rightToMaybe $ do
    unicode <- decodeBase16 hex
    return $ encodeUtf8 unicode

writeHex :: ByteString -> Text
writeHex = encodeBase16 . decodeUtf8

readBase64 :: Text -> Maybe ByteString
readBase64 base64 = rightToMaybe $ decodeBase64 (encodeUtf8 base64)

readFileBase64 :: String -> IO (Maybe ByteString)
readFileBase64 fileName = readBase64 . joinString <$> readFile fileName
  where
    joinString :: String -> Text
    joinString t = T.pack $ concat $ lines t

readLinesBase64 :: String -> IO [ByteString]
readLinesBase64 fileName = readLines' <$> readFile fileName
  where
    readLines' :: String -> [ByteString]
    readLines' input = catMaybes $ readBase64 . T.pack <$> lines input

xor :: ByteString -> ByteString -> ByteString
xor x y = pack $ BS.zipWith Bits.xor x y

xorHex :: Text -> Text -> Maybe Text
xorHex x y = do
  x' <- readHex x
  y' <- readHex y
  return $ writeHex $ x' `xor` y'

stripCtrl :: Text -> Text
stripCtrl = T.filter (\x -> (not . isControl) x || x == '\n')

fromQueryString :: Text -> Maybe (OMap Text Text)
fromQueryString "" = Just OM.empty
fromQueryString input = parse input
  where
    parse :: Text -> Maybe (OMap Text Text)
    parse = fmap OM.fromList . mapM splitKeyVal . splitFields

    splitFields :: Text -> [Text]
    splitFields = T.splitOn "&"

    splitKeyVal :: Text -> Maybe (Text, Text)
    splitKeyVal = toPair . T.splitOn "="
      where
        toPair :: [Text] -> Maybe (Text, Text)
        toPair [x, y] = Just (x, y)
        toPair _ = Nothing

toQueryString :: OMap Text Text -> Text
toQueryString = T.intercalate "&" . fmap encodeKV . OM.assocs
  where
    encodeKV :: (Text, Text) -> Text
    encodeKV (k, v) = stripMeta k <> "=" <> stripMeta v

    stripMeta :: Text -> Text
    stripMeta = T.filter (\x -> x /= '=' && x /= '&')