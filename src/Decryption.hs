module Decryption (decryptAES128ECB, decryptAES128CBC) where

import Prelude hiding (filter, concat)
import Crypto.Cipher.AES (AES128)
import Crypto.Cipher.Types (cipherInit, ecbDecrypt)
import Crypto.Error (maybeCryptoError)
import Data.ByteString (ByteString, concat)
import Data.Char (isControl)
import Data.Either.Combinators (rightToMaybe)
import Data.List (foldl')
import Data.Text (Text, filter)
import Data.Text.Encoding (decodeUtf8')
import Util (blocks, xor, stripCtrl)

decryptAES128ECB :: ByteString -> ByteString -> Maybe Text
decryptAES128ECB key ciphertext = do
  aes <- maybeCryptoError $ cipherInit key :: Maybe AES128
  let decrypted = ecbDecrypt aes ciphertext
  let decoded = rightToMaybe $ decodeUtf8' decrypted
  stripCtrl <$> decoded

decryptAES128CBC :: ByteString -> ByteString -> ByteString -> Maybe Text
decryptAES128CBC key iv ciphertext = do
  aes <- maybeCryptoError $ cipherInit key :: Maybe AES128
  let theBlocks = blocks 16 ciphertext
  let decryptedBlocks = snd $ foldl' (chainDecrypt aes) (iv, []) theBlocks
  let decoded = rightToMaybe $ decodeUtf8' $ concat decryptedBlocks
  stripCtrl <$> decoded
  where
    chainDecrypt :: AES128 -> (ByteString, [ByteString]) -> ByteString -> (ByteString, [ByteString])
    chainDecrypt key (prevBlock, result) block = 
      let decryptedBlock = prevBlock `xor` ecbDecrypt key block
      in  (block, result ++ [decryptedBlock])