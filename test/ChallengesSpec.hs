module ChallengesSpec where

import Test.Hspec
import Data.Text (Text)
import qualified Data.Text as T
import Challenges (challenges, Challenge)

spec :: Spec
spec = do

  it "solves challenge 1" $ do
    "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t" `shouldBeResultOfChallenge` 1

  it "solves challenge 2" $ do
    "746865206b696420646f6e277420706c6179" `shouldBeResultOfChallenge` 2

  it "solves challenge 3" $ do
    "Cooking MC's like a pound of bacon" `shouldBeResultOfChallenge` 3

  it "solves challenge 4" $ do
    "Now that the party is jumping" `shouldBeResultOfChallenge` 4

  it "solves challenge 5" $ do
    "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
      `shouldBeResultOfChallenge` 5

  it "solves challenge 6" $ do
    expected <- T.pack <$> readFile "test/vanilla-ice.txt"
    expected `shouldBeResultOfChallenge` 6

  it "solves challenge 7" $ do
    expected <- T.pack <$> readFile "test/vanilla-ice.txt"
    expected `shouldBeResultOfChallenge` 7

  it "solves challenge 8" $ do
    "132" `shouldBeResultOfChallenge` 8

  it "solves challenge 9" $ do
    "YELLOW SUBMARINE\x04\x04\x04\x04" `shouldBeResultOfChallenge` 9

  it "solves challenge 10" $ do
    expected <- T.pack <$> readFile "test/vanilla-ice.txt"
    expected `shouldBeResultOfChallenge` 10

  it "solves challenge 11" $ do
    let challenge = challenges !! 10
    Just actual <- challenge
    T.unpack actual `shouldStartWith` "1: Expected: ECB - Actual: ECB\n2: Expected: CBC - Actual: CBC\n3: Expected: ? - Actual: "

  it "solves challenge 12" $ do
    let expected = "Rollin' in my 5.0\nWith my rag-top down so my hair can blow\nThe girlies on standby waving just to say hi\nDid you stop? No, I just drove by\n"
    expected `shouldBeResultOfChallenge` 12

  it "solves challenge 13" $ do
    "admin" `shouldBeResultOfChallenge` 13

shouldBeResultOfChallenge :: Text -> Int -> IO ()
shouldBeResultOfChallenge expected i = do
  let challenge = challenges !! (i-1)
  Just actual <- challenge
  actual `shouldBe` expected