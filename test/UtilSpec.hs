module UtilSpec where

import Test.Hspec

import qualified Data.Map.Ordered as OM
import Data.Text (Text)
import Util (fromQueryString, toQueryString)

spec :: Spec
spec = do
  context "fromQueryString" $ do
    it "parses regular query string" $ do
      let input = "foo=bar&baz=qux&zap=zazzle"
      let expected = OM.fromList [("foo", "bar"), ("baz", "qux"), ("zap", "zazzle")]
      fromQueryString input `shouldBe` Just expected

    it "parses query string with single pair" $ do
      let input = "foo=bar"
      let expected = OM.fromList [("foo", "bar")]
      fromQueryString input `shouldBe` Just expected

    it "parses empty query string" $ do
      let input = ""
      fromQueryString input `shouldBe` Just OM.empty

    it "fails to parse query string with invalid structure (1)" $ do
      let input = "&&"
      fromQueryString input `shouldBe` Nothing

    it "fails to parse query string with invalid structure (2)" $ do
      let input = "foo&bar=baz"
      fromQueryString input `shouldBe` Nothing

  context "toQueryString" $ do
    it "encodes map in query string format" $ do
      let input = OM.fromList [("foo", "bar"), ("baz", "qux"), ("zap", "zazzle")]
      let expected = "foo=bar&baz=qux&zap=zazzle"
      toQueryString input `shouldBe` expected

    it "encodes empty map as empty string" $ do
      toQueryString OM.empty `shouldBe` ""

    it "disallows metacharacters in keys and values" $ do
      let input = OM.fromList [("foo", "bar"), ("role=admin&baz", "qux"), ("zap", "zazzle&role=admin")]
      let expected = "foo=bar&roleadminbaz=qux&zap=zazzleroleadmin"
      toQueryString input `shouldBe` expected