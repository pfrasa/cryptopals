module Main where

import System.IO (hFlush, stdout)
import Data.Text (Text)
import qualified Data.Text as T
import Challenges (challenges)

main :: IO ()
main = do
    putStr $ "Which challenge to solve? (1-" ++ show (length challenges) ++ ") "
    hFlush stdout 
    line <- getLine
    result <- challenges !! (read line - 1)
    putStrM result

putStrM :: Maybe Text -> IO ()
putStrM (Just x) = putStrLn (T.unpack x)
putStrM Nothing = putStrLn "error"